package com.wr1.wr1.ListItems

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.stfalcon.frescoimageviewer.ImageViewer
import com.wr1.wr1.Adapters.PostAdapter
import com.wr1.wr1.Layouts.PollAnswerLayout
import com.wr1.wr1.Layouts.SocialLayout
import com.wr1.wr1.Models.Club
import com.wr1.wr1.Models.Post
import com.wr1.wr1.R
import org.jetbrains.anko.layoutInflater

/**
 * Created by charlesvonlehe on 12/21/17.
 */

class PostPollListItem(itemView: View, likeListener: SocialLayout.LikeListener, source:Post.PostSource) : PostListItem(itemView, likeListener, source), PollAnswerLayout.VoteListener {


    var answerLayout:LinearLayout? = null
    var imageView:ImageView? = null
    var questionTextView:TextView? = null
    var answerLayouts = ArrayList<PollAnswerLayout>()

    init {
        imageView = itemView.findViewById(R.id.image_view)
        answerLayout = itemView.findViewById(R.id.answer_container_layout)
        questionTextView = itemView.findViewById(R.id.question_text_view)
    }


    override fun populate(forPost: Post, forClub: Club?, postDeleteListener: PostDeleteListener?) {
        super.populate(forPost, forClub, postDeleteListener)
        val context = itemView.context ?: return
        if (forPost.title.length > 0) {
            questionTextView?.text = forPost.title
            questionTextView?.visibility = View.VISIBLE
        }else {
            questionTextView?.visibility = View.GONE
        }


        if (forPost.url != null && imageView != null) {
            imageView?.visibility = View.VISIBLE
            Glide.with(context).load(forPost.url).into(imageView)
            imageView?.setOnClickListener {
                if (forPost.url != null) {
                    val array = ArrayList<String>()
                    array.add(forPost.url!!)
                    ImageViewer.Builder(context, array).setStartPosition(0).show()
                }
            }
        }else {
            imageView?.visibility = View.GONE
        }


        if (answerLayouts.size <= 0) {
            for (i in 1..forPost.postItems.count()) {
                val postItem = forPost.postItems.get(i - 1)
                val pollAnswerLayout = context.layoutInflater.inflate(R.layout.poll_answer_layout, null) as PollAnswerLayout
                pollAnswerLayout.populate(postItem, i, forPost, this)
                answerLayout?.addView(pollAnswerLayout)
                answerLayouts.add(pollAnswerLayout)
            }
        }



    }

    fun resetVotes (forPost: Post) {
        val context = itemView.context ?: return
        for (i in 1..answerLayouts.count()) {
            val postItem = forPost.postItems.get(i - 1)
            val pollAnswerLayout = answerLayouts.get(i - 1)
            pollAnswerLayout.populate(postItem, i, forPost, this)
        }
    }

    override fun voted(forPost: Post) {
        resetVotes(forPost)
    }

}