package com.wr1.wr1.ListItems

import android.content.Context
import android.content.res.Resources
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.stfalcon.frescoimageviewer.ImageViewer
import com.wr1.wr1.Adapters.PostAdapter
import com.wr1.wr1.Helpers.AnalyticsHelper
import com.wr1.wr1.Helpers.MediaHelper
import com.wr1.wr1.Layouts.SocialLayout
import com.wr1.wr1.Models.Club
import com.wr1.wr1.Models.Post
import com.wr1.wr1.R

/**
 * Created by charlesvonlehe on 12/21/17.
 */
class PostImageListItem(itemView: View, likeListener: SocialLayout.LikeListener, source: Post.PostSource) : PostListItem(itemView, likeListener, source) {
    var imageView: ImageView? = null


    init {
        imageView = itemView.findViewById(R.id.image_view)
    }

    override fun populate(forPost: Post, forClub: Club?, postDeleteListener: PostDeleteListener?) {
        super.populate(forPost, forClub, postDeleteListener)

        val context = itemView.context ?: return
        if (forPost.url != null && imageView != null) {
            val height = forPost.getAspectHeight()
            val screenWidth = Resources.getSystem().displayMetrics.widthPixels
            if (height != null && height > 0 && screenWidth > 0) {
                imageView?.layoutParams?.height = height
                imageView?.requestLayout()
                Glide.with(context).load(forPost.url).apply(RequestOptions().fitCenter().placeholder(R.drawable.image_placeholder)).into(imageView)
            }else {

                Glide.with(context).load(forPost.url).apply(RequestOptions().placeholder(R.drawable.image_placeholder)).into(imageView)
            }
            imageView?.setOnClickListener {
                if (forPost.url != null) {
                    MediaHelper.displayPhoto(context, forPost.url!!)

                    AnalyticsHelper.shared().firePostDetailShowImageEvent(forPost.clubId, forPost.id)
                }
            }
        }


    }
}