package com.wr1.wr1.ListItems

import android.content.Context
import android.content.res.Resources
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.wr1.wr1.Activities.ExoPlayerActivity
import com.wr1.wr1.Activities.VLCPlayerActivity
import com.wr1.wr1.Activities.VideoPlayer
import com.wr1.wr1.Activities.ViewStreamActivity
import com.wr1.wr1.Adapters.PostAdapter
import com.wr1.wr1.Initialization.Constants
import com.wr1.wr1.Layouts.SocialLayout
import com.wr1.wr1.Models.Club
import com.wr1.wr1.Models.Post
import com.wr1.wr1.Models.Stream
import com.wr1.wr1.R
import im.ene.toro.ToroPlayer
import im.ene.toro.ToroUtil
import im.ene.toro.exoplayer.SimpleExoPlayerViewHelper
import im.ene.toro.media.PlaybackInfo
import im.ene.toro.widget.Container

/**
 * Created by charlesvonlehe on 12/21/17.
 */
open class PostVideoListItem(itemView: View, likeListener: SocialLayout.LikeListener, source: Post.PostSource) : PostListItem(itemView, likeListener, source),ToroPlayer {


    var helper:SimpleExoPlayerViewHelper? = null
    var playerView:SimpleExoPlayerView? = null
    var thumbnailImageView:ImageView? = null
    var post:Post? = null
    var videoContainerLayout:RelativeLayout? = null
    var thumbnailLayout:RelativeLayout? = null

    init {

        playerView = itemView.findViewById(R.id.player_view)
        thumbnailImageView = itemView.findViewById(R.id.thumbnail_image_view)
        thumbnailLayout = itemView.findViewById(R.id.thumbnail_layout)
        playerView?.useController = false
        playerView?.visibility = View.INVISIBLE
        videoContainerLayout = itemView.findViewById(R.id.video_container_layout)

    }

    override fun populate(forPost: Post, forClub:Club?, postDeleteListener: PostDeleteListener?) {
        super.populate(forPost, forClub, postDeleteListener)

        val context = itemView.context ?: return
        println("POST_VIDEO_URL: " + forPost.url)
        post = forPost
        val height = forPost.getAspectHeight()
        val screenWidth = Resources.getSystem().displayMetrics.widthPixels
        if (height != null && height > 0 && screenWidth > 0) {
            thumbnailLayout?.layoutParams?.height = height
            thumbnailLayout?.requestLayout()
            playerView?.layoutParams?.height = height
            playerView?.requestLayout()
            loadThumbnail(forPost, context)
        }else {
            loadThumbnail(forPost, context)
        }

        videoContainerLayout?.setOnClickListener {
            println("STREAM_STATE: " + forPost.streamState)
            val post = forPost ?: return@setOnClickListener
            if (post.streamState == Stream.State.ENDED) {
                VLCPlayerActivity.display(context, post)
            }else {
                VideoPlayer.display(context, post, null)
            }

        }
    }

    private fun loadThumbnail (forPost:Post, context: Context) {
        if (forPost.thumbnailUrl != null) {
            Glide.with(context).load(forPost.thumbnailUrl).apply(RequestOptions().centerCrop()).into(thumbnailImageView)
        }else {
            Glide.with(context).load(R.drawable.black_layout).apply(RequestOptions().centerCrop()).into(thumbnailImageView)
        }
    }

    override fun isPlaying(): Boolean {
        return  helper != null && helper!!.isPlaying
    }

    override fun getPlayerView(): View {
        if (playerView != null) {
            return playerView!!
        }else {
            return SimpleExoPlayerView(itemView.context)
        }
    }

    override fun pause() {
        helper?.pause()
    }

    override fun wantsToPlay(): Boolean {
        return ToroUtil.visibleAreaOffset(this, itemView.parent) >= 0.85 && post != null && post!!.currentUserHasAccess()
    }

    override fun play() {
        playerView?.visibility = View.VISIBLE
        thumbnailLayout?.visibility = View.GONE
        helper?.play()

    }


    override fun getCurrentPlaybackInfo(): PlaybackInfo {
        if (helper != null) {
           return helper!!.latestPlaybackInfo
        }else {
            return PlaybackInfo()
        }
    }

    override fun release() {
        helper?.release()
        helper = null
    }

    override fun initialize(container: Container, playbackInfo: PlaybackInfo?) {
        if (helper == null && post?.url != null) {
            helper = SimpleExoPlayerViewHelper(container, this, Uri.parse(post?.url))
        }
        helper?.initialize(playbackInfo)
        playerView?.player?.volume = 0f
    }

    override fun getPlayerOrder(): Int {
        return adapterPosition
    }

    override fun onSettled(container: Container?) {
    }

}