package com.wr1.wr1.ListItems

import android.app.AlertDialog
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.wr1.wr1.Activities.*
import com.wr1.wr1.Helpers.AnalyticsHelper
import com.wr1.wr1.Helpers.MethodHelper
import com.wr1.wr1.Initialization.Constants
import com.wr1.wr1.Layouts.SmallProfileLayout
import com.wr1.wr1.Layouts.SocialLayout
import com.wr1.wr1.Models.Chat
import com.wr1.wr1.Models.Club
import com.wr1.wr1.Models.Post
import com.wr1.wr1.Models.User
import com.wr1.wr1.R

open class PostListItem(itemView: View, likeListener: SocialLayout.LikeListener, source: Post.PostSource) : RecyclerView.ViewHolder(itemView) {


    var container_layout: View? = null
    var socialLayout: SocialLayout? = null
    var profileLayout: SmallProfileLayout? = null
    var textView: TextView? = null
    private var source = Post.PostSource.discover
    var emptyLayout: RelativeLayout? = null
    var instagramImageView: ImageView? = null
    var shouldDisplayPostDetails = true
    var fragment: Fragment? = null
    var unlockLayout: RelativeLayout? = null
    var flagDeleteImageView: ImageView? = null
    var likeListener: SocialLayout.LikeListener? = null

    init {
        container_layout = itemView.findViewById(R.id.container_layout)
        socialLayout = itemView.findViewById(R.id.social_layout)
        socialLayout?.initialize(itemView)
        profileLayout = itemView.findViewById(R.id.profile_layout)
        profileLayout?.initialize(itemView)
        textView = itemView.findViewById(R.id.text_view)
        emptyLayout = itemView.findViewById(R.id.empty_layout)
        instagramImageView = itemView.findViewById(R.id.instagram_image_view)
        unlockLayout = itemView.findViewById(R.id.unlock_layout)
        flagDeleteImageView = itemView.findViewById(R.id.flag_delete_image_view)
        this.likeListener = likeListener
        this.source = source
    }

    open fun populate(forPost: Post, forClub:Club?, postDeleteListener: PostDeleteListener?) {
        if (source == Post.PostSource.artist && textView != null) {
            textView?.autoLinkMask = Linkify.WEB_URLS
            textView?.movementMethod = LinkMovementMethod.getInstance()
            Linkify.addLinks(textView, Linkify.ALL)
        }
        container_layout?.setOnClickListener {
            if (shouldDisplayPostDetails) {
                PostDetailsActivity.display(itemView.context, forPost, source, null, null)
            }
        }
        if (forPost.customData != null && forPost.customData!!.contains(Constants.URLs.instagramURL)) {
            flagDeleteImageView?.visibility = View.GONE
            instagramImageView?.visibility = View.VISIBLE
        } else {
            flagDeleteImageView?.visibility = View.VISIBLE
            instagramImageView?.visibility = View.GONE
            flagDeleteImageView?.setOnClickListener { flagDeleteCicked(forPost, postDeleteListener) }
        }
        profileLayout?.populate(itemView.context, forPost.owner)

        socialLayout?.populate(itemView.context, forPost, likeListener!!, forClub?.name)

        profileLayout?.avatarImageView?.setOnClickListener {
            clickedUser(forPost, itemView.context)
        }

        profileLayout?.userNameTextView?.setOnClickListener {
            clickedUser(forPost, itemView.context)
        }

        if (forPost.text.isNotEmpty()) {
            textView?.visibility = View.VISIBLE
            textView?.text = forPost.text
        } else {
            textView?.visibility = View.GONE
        }
        unlockLayout?.visibility = if (forPost.currentUserHasAccess()) View.GONE else View.VISIBLE
        unlockLayout?.setOnClickListener {
            unlockLayoutClicked(forPost, itemView.context)
        }

    }


    interface PostDeleteListener {
        fun didDelete(post: Post)
    }



    private fun unlockLayoutClicked(forPost: Post, context: Context) {
        if (!User.userExists()) {
            LoginActivity.display(context)
        } else if (forPost.coinPrice > User.current()!!.totalCoins) {
            MethodHelper.showAlert(context, "Not Enough Coins", "You do not have enough coins to unlock this post. You need to subscribe to the club to view the post.")
        } else {
            forPost.unlock(context) { success ->
                if (success) {
                    unlockLayout?.visibility = View.GONE
                } else {
                    MethodHelper.showToast(context, "Error unlocking content")
                }
            }
        }
    }

    private fun flagDeleteCicked(forPost: Post, deleteListener: PostDeleteListener?) {
        val currentUser = User.current() ?:return
        if (currentUser.owns(forPost.clubId)) {
            if (forPost.postByArtist || forPost.postByManager) {
                MethodHelper.displayPopupMenu(itemView.context, flagDeleteImageView!!, arrayListOf("Flag", "Delete", if (forPost.pinned) "Unpin" else "Pin")) { selectedIndex ->
                    when (selectedIndex) {
                        0 -> flagClicked(forPost)
                        1 -> deleteClicked(forPost, deleteListener)
                        2 -> pinClicked(forPost)
                    }
                }
            }else {
                MethodHelper.displayPopupMenu(itemView.context, flagDeleteImageView!!, arrayListOf("Flag", "Delete")) { selectedIndex ->
                    when (selectedIndex) {
                        0 -> flagClicked(forPost)
                        1 -> deleteClicked(forPost, deleteListener)
                    }
                }
            }
        } else if (forPost.ownedByCurrentUser()) {
            MethodHelper.displayPopupMenu(itemView.context, flagDeleteImageView!!, arrayListOf("Flag", "Delete")) { selectedIndex ->
                when (selectedIndex) {
                    0 -> flagClicked(forPost)
                    1 -> deleteClicked(forPost, deleteListener)
                }
            }
        } else {
            flagClicked(forPost)
        }

    }

    private fun pinClicked(forPost: Post) {
        if (forPost.pinned) {
            forPost.unpin(itemView.context) { success ->
                if (success) MethodHelper.showToast(itemView.context, "Post unpinned")
                else MethodHelper.showToast(itemView.context, "Error unpinning post")

            }
        }else {
            forPost.pin(itemView.context) { success ->
                if (success) MethodHelper.showToast(itemView.context, "Post pinned to top")
                else MethodHelper.showToast(itemView.context, "Error pinning post")

            }
        }
    }

    private fun deleteClicked(forPost: Post, deleteListener: PostDeleteListener?) {
        AlertDialog.Builder(itemView.context).setTitle("Delete").setMessage("You are about to delete this comment. Would you like to continue?").setPositiveButton("Ok") { _, _ ->
            forPost.delete(itemView.context) { success ->
                if (success) {
                    deleteListener?.didDelete(forPost)
                } else {
                    MethodHelper.showToast(itemView.context, "Error deleting comment")
                }
            }
        }.setNegativeButton("Cancel") { _, _ -> }.create().show()
    }

    private fun flagClicked(forPost: Post) {
        AlertDialog.Builder(itemView.context).setTitle("Flag").setMessage("You are about to flag this post. Would you like to continue?").setPositiveButton("Ok") { _, _ ->
            forPost.flag(itemView.context) { success ->
                if (success) {
                    MethodHelper.showToast(itemView.context, "Flagged Post")
                } else {
                    MethodHelper.showToast(itemView.context, "Error flagging post")
                }
            }
        }.setNegativeButton("Cancel") { _, _ -> }.create().show()
    }

    private fun clickedUser(forPost: Post, context: Context) {
        if (fragment != null) {

            if (source == Post.PostSource.fan) {
                AlertDialog.Builder(context).setTitle(forPost.owner?.name).setPositiveButton("View Profile") { _, _ ->
                    if (forPost.owner != null) {
                        ProfileActivity.display(context, forPost.owner!!)
                    }
                }.setNegativeButton("Chat") { _, _ ->
                    if (forPost.owner != null) {
                        Chat.createChat(true, forPost.owner!!) { chat ->
                            if (chat != null) {
                                ChatActivity.display(itemView.context, chat, forPost.clubId, forPost.owner)
                            }
                        }
                    }
                }.setNeutralButton("Cancel") { _, _ -> }.create().show()
            } else {
                forPost.getClub(context) { club ->
                    AnalyticsHelper.shared().fireClubWallViewEvent(fragment!!, forPost.clubId)
                    if (club != null) ClubActivity.display(context, club)
                }
            }
        }
    }

}